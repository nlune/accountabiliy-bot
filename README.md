# accountabiliy-bot

A quickly put together bot to help track points in a telegram accountability group

## Getting started

Get the api_id and api_hash from https://my.telegram.org/apps

Get the bot token from Botfather on Telegram

Add the bot to your accountability points group, and it will keep track of your daily points and gamify the process a bit for you :)

