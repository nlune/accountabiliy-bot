import os
from telethon import TelegramClient, events
from telethon import Button
import asyncio
from datetime import datetime
from pathlib import Path
from tinydb import TinyDB, Query, where
import emoji
from art import *
import logging
from dotenv import load_dotenv
env_path = Path('.')/'.env'
load_dotenv(dotenv_path=env_path)

logging.basicConfig(level=logging.INFO)


class AccountabilityBot: 

    def __init__(self, session_user:str, api_id:int, api_hash:str, db_name:str='users.json'):

        logging.info("Initializing...")

        self.client = TelegramClient(session_user, api_id, api_hash)
        self.client.start() 


        self.db = TinyDB(db_name)


    async def user_in_db(self, user_id): 
            if self.db.search(Query().user_id==user_id):
                return True
            return False

    async def start_handler(self, event):
        keyboard = [Button.inline('Options', b'options'), Button.inline('User Stats', b'stats')]
        await event.respond('Send any message to be added to the database and start collecting points. Enter a number to add the points. To look at your stats and settings privately, talk to me @accountable_bot. Press /start or /info for this message.', buttons=keyboard)


    async def response_handler(self, event):
        msg = event.raw_text
        if not msg.startswith('/'):
            user_id = event.sender_id
            user_exists = await self.user_in_db(user_id)

            user = await event.get_sender()
            first_name = user.first_name

            if user_exists:
                if self.db.search(Query().user_id==user_id)[0]["emoji"] == 1: 
                    msg = emoji.demojize(msg)
                    if msg.startswith(':'): 
                        self.db.update({'emoji': msg}, Query().user_id==user_id)
                        await event.respond(f"Emoji set for {first_name} " + emoji.emojize(msg)) 
                    else:
                        await event.respond("send me an emoji")
                    return



                if msg.replace('-', '',1).replace('.','',1).isdigit():
                    old_pts = self.db.search(Query().user_id==user_id)[0]["totalPts"]
                    emo = self.db.search(Query().user_id==user_id)[0]["emoji"]

                    new_pts = float(msg) if '.' in msg else int(msg)

                    self.db.update({'totalPts': old_pts + new_pts}, Query().user_id==user_id)
                    response = f"New points registered for {first_name} {emo if emo else ''} ===>  {new_pts} points added."
                    await event.respond(emoji.emojize(response))
            else:
                self.db.insert({'user_id': user_id, 'firstName': first_name, 'totalPts': 0, 'totalStars': 0, 'totalGifts': 0,  'emoji': ''})
                await event.respond("You have been added to the database. Enter your new points to start collecting. ")
                print(self.db.search(Query().user_id==user_id))
            




    async def option_handler(self, event): 
        keyboard = [Button.inline('Set Emoji', b'emoji'), Button.inline('Remove data', b'delete')]
        await event.respond("What would you like to do?", buttons=keyboard)

    async def stats_handler(self, event): 
        keyboard = [Button.inline('Display Stats', b'display_stats')]
        await event.respond("You can check your totals by clicking button below or via /display_stats.\n\nFor every 10 points you can get a star with /get_star, and you can get a reward with 3 stars with /get_gift.\n\nTo swap all points to stars use /week_end_conversion.\n\nTo claim your reward use /claim_reward", buttons=keyboard)

    async def display_stats(self, event): 
        user_id = event.sender_id
        user_exists = await self.user_in_db(user_id)
       
        user = await event.get_sender()
        first_name = user.first_name
        if user_exists: 
            emo = self.db.search(Query().user_id==user_id)[0]["emoji"]
            emo = emoji.emojize(emo)
            pts = self.db.search(Query().user_id==user_id)[0]["totalPts"] 
            stars = self.db.search(Query().user_id==user_id)[0]["totalStars"] 
            r = self.db.search(Query().user_id==user_id)[0]["totalGifts"] 
            await event.respond(f"**Stats for {first_name}** {emo if emo else ''} \n\n**{int(pts) if pts.is_integer() else pts}** " + u'\U0001F41E' + f" points,  **{int(stars) if stars.is_integer() else stars}** " + u'\U0001F31F'  + f" stars,  **{r}** " + u'\U0001F381' + " rewards \n\n10 pts ==> 1 star /get_star \n3 stars ==> 1 reward /get_gift") 
            




    async def emoji_handler(self, event): 
        user_id = event.sender_id
        user_exists = await self.user_in_db(user_id)
        if user_exists: 
            await event.respond("To set your emoji, send the emoji you would like to represent you.")
            self.db.update({'emoji': 1}, Query().user_id==user_id)
        else: 
            await event.respond("you're not yet a user")

    async def remove_user_handler(self, event): 
        user_id = event.sender_id
        user_exists = await self.user_in_db(user_id)
        if user_exists: 
            name = self.db.search(Query().user_id==user_id)[0]["firstName"] 
            self.db.remove(where('user_id') == user_id)
            await event.respond(f"Goodbye {name}, you have been removed.")
        else: 
            await event.respond("You're not in the database.")


    async def get_star(self, event): 
        user_id = event.sender_id
        user_exists = await self.user_in_db(user_id)
        if user_exists: 
            pts = self.db.search(Query().user_id==user_id)[0]["totalPts"] 
            stars = self.db.search(Query().user_id==user_id)[0]["totalStars"] 

            if pts >= 10: 
                self.db.update({'totalPts': pts - 10}, Query().user_id==user_id)
                self.db.update({'totalStars': stars + 1 }, Query().user_id==user_id)
                await event.respond("You bought a star! " + u'\U0001F31F')
            else: 
                await event.respond("keep working hard, you don't have enough points yet. 10 pts ==> 1 star")





    async def get_gift(self, event): 
        user_id = event.sender_id
        user_exists = await self.user_in_db(user_id)
        if user_exists: 
            rewards = self.db.search(Query().user_id==user_id)[0]["totalGifts"] 
            stars = self.db.search(Query().user_id==user_id)[0]["totalStars"] 

            if stars >= 3: 
                self.db.update({'totalGifts': rewards + 1}, Query().user_id==user_id)
                self.db.update({'totalStars': stars - 3 }, Query().user_id==user_id)
                await event.respond("You bought a gift! " + u'\U0001F381' + " say /claim_reward to use it.")
            else: 
                await event.respond("keep working hard, you don't have enough stars yet. 3 stars ==> 1 gift")

    async def claim_reward(self, event): 
        user_id = event.sender_id
        user_exists = await self.user_in_db(user_id)
        if user_exists: 
            rewards = self.db.search(Query().user_id==user_id)[0]["totalGifts"] 
            if rewards >= 1: 
                self.db.update({'totalGifts': rewards - 1}, Query().user_id==user_id)
                resp = ":sparkles: Here's a random piece of art. You can choose how to reward yourself ;) :sparkles:"
                resp = emoji.emojize(resp)
                art = randart()
                await event.respond(resp)
                await event.respond(art)
            else:
                await event.respond("You have to buy a reward before using it. /get_gift" )


    async def week_end_conversion(self, event):
        user_id = event.sender_id
        user_exists = await self.user_in_db(user_id)
        if user_exists: 
            pts = self.db.search(Query().user_id==user_id)[0]["totalPts"] 
            stars = self.db.search(Query().user_id==user_id)[0]["totalStars"] 

            if pts >= 10: 
                self.db.update({'totalPts': 0}, Query().user_id==user_id)
                self.db.update({'totalStars': stars +  pts/10 }, Query().user_id==user_id)
                await event.respond(f"Nice work, that's {pts/10} stars this week " + u'\U0001F31F')
            else: 
                await event.respond("keep working hard, you don't have enough points yet. 10 pts ==> 1 star")








        

    def run(self):
        with self.client: 
            self.client.add_event_handler(self.start_handler, events.NewMessage(pattern='/start|/info'))
            self.client.add_event_handler(self.get_star, events.NewMessage(pattern='/get_star'))
            self.client.add_event_handler(self.get_gift, events.NewMessage(pattern='/get_gift'))
            self.client.add_event_handler(self.display_stats, events.NewMessage(pattern='/display_stats'))
            self.client.add_event_handler(self.week_end_conversion, events.NewMessage(pattern='/week_end_conversion'))
            self.client.add_event_handler(self.claim_reward, events.NewMessage(pattern='/claim_reward'))
            self.client.add_event_handler(self.response_handler, events.NewMessage(outgoing=False))
            self.client.add_event_handler(self.option_handler, events.CallbackQuery(data=b'options'))
            self.client.add_event_handler(self.stats_handler, events.CallbackQuery(data=b'stats'))
            self.client.add_event_handler(self.emoji_handler, events.CallbackQuery(data=b'emoji'))
            self.client.add_event_handler(self.display_stats, events.CallbackQuery(data=b'display_stats'))
            self.client.add_event_handler(self.remove_user_handler, events.CallbackQuery(data=b'delete'))

            self.client.run_until_disconnected()



if __name__=='__main__':
    session_user = 'accbot'
    api_id = os.getenv('APP_API_ID')
    api_hash = os.getenv('APP_API_HASH')
    bot = AccountabilityBot(session_user, api_id, api_hash)
    bot.run()


